<!---
maarten0912/maarten0912 is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->

![Profile views](https://gpvc.arturio.dev/maarten0912)

<h1 align="center">Hi 👋, I'm Maarten Meijer</h1>
<h3 align="center">A passionate Computer Science enthousiast from the Netherlands</h3>

- 👀 Interested in **Software Engineering**, **Data Science** and **Cybersecurity**
- 🌱 Currently learning **Deep Learning** in Computer Vision using TAO toolkit and TensorRT
- 🎓 Following the Master's course Computer Science at the University of Twente, with the specialization of Data Science
- 👔 Working part-time as a Data Scientist at Nedap
- 📫 Reach me here: [Private mail](mailto://maarten0912@gmail.com) | [University mail](mailto://m.p.meijer@student.utwente.nl) | [Linkedin](https://www.linkedin.com/in/maartenmeijer0/)

<!-- ## ✉️ Find me on:

<p align="center">
 <a href="https://charalambosioannou.github.io/" target="_blank" rel="noopener noreferrer"> <img src="https://raw.githubusercontent.com/iconic/open-iconic/master/svg/globe.svg" alt="Python" height="40" style="vertical-align:top; margin:4px"> </a>
 <a href="https://linkedin.com/in/charalambosioannou" target="_blank" rel="noopener noreferrer"> <img src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" alt="Python" height="40" style="vertical-align:top; margin:4px"></a>
 <a href="mailto:cioannou1997@gmail.com"> <img src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/gmail.svg" alt="Python" height="40" style="vertical-align:top; margin:4px"></a>
</p> -->

<!-- ## 🧰 Languages and Tools:

<p align="center">
  <a href="https://opencv.org/">
    <img src="https://www.vectorlogo.zone/logos/opencv/opencv-icon.svg" alt="opencv" width="40" height="40"/>
  </a>

  <a href="https://www.python.org">
    <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/>
  </a> 

  <a href="https://pytorch.org/">
    <img src="https://www.vectorlogo.zone/logos/pytorch/pytorch-icon.svg" alt="pytorch" width="40" height="40"/>
  </a> 

  <a href="https://git-scm.com/">
      <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/>
  </a> 
</p> -->
